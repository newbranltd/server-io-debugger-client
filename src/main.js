/**
 * Main this is different from the original implementation,
 * everything using the default options, the only thing is
 * it listen to a custom event to get the nsp endpoint
 * this will force to over take the console.log method
 */
(function(window , navigator, StackTrace, io)
{
  'use strict';

  // We start by listing to a custom event trigger by the app
  // to tell us the nsp location
  window.addEventListener('serverIoDebuggerStart', function(e) => {
    if (e.nsp) {
      var nsp = io(e.nsp);
      /**
       * @param {object} payload send to the server
       */
      var send = function(payload) {
        payload.browser = navigator.userAgent;
        payload.location = window.location.href;
        nsp.emit('debugging', payload);
      };

      /**
       * listen to the init connection
       * then we store the message in the sessionStorage for pick up
       * also trigger a custom message for the other to listen to
       */
      nsp.on('hello', function (msg) {
        try {
          sessionStorage.setItem('serverIoDebuggerInited', msg);
          // create a custom event
          var evt = new CustomEvent('serverIoDebuggerInited');
          evt.msg = msg;
          window.dispatchEvent(evt);
          // console.info('debugger init connection: ' , msg);
        } catch (e) {}
      });

      /**
       * core implementation
       */
      window.onerror = function(msg, file, line, col, error) {
        // callback is called with an Array[StackFrame]
        StackTrace.fromError(error)
          .then(function(data) {
            send({msg: data, from: 'error', color: 'warning'});
          })
          .catch(function(err) {
            send({msg: err, from: 'catch onerror', color: 'debug'});
          });
      };

      /**
       * added on V1.4.0
       */
      window.onunhandledrejection = function(e) {
        StackTrace.fromError(e)
          .then(function(data) {
            send({msg: data, from: 'onunhandledrejection', color: 'warning'});
          })
          .catch(function(err) {
            send({msg: err, from: 'catch onunhandledrejection', color: 'debug'});
          });
      }

      // overwrite the console.log
      window['console']['log'] = function() {
        var args = Array.prototype.slice.call(arguments);
        send({msg: args, from: 'debug'});
      };

    }
  });

})(window , navigator, StackTrace, io);

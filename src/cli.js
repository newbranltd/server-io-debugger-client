/**
 * Main interface to connect to the debugger broadcaster
 */
const meow = require('meow');
const log = require('fancy-log');
const chalk = require('chalk');
const io = require('socket.io-client');
/**
 * make sure it's an object
 * @param {mixed} opt data
 * @return {mixed} object or original data
 */
const parse = opt => {
  try {
    return typeof opt === 'object' ?  opt : JSON.parse(opt);
  } catch (e) {}
  return opt;
};
/**
 * Apply format and filter etc
 * @param {mixed} opt data
 * @return {undefined} nothing
 */
const display = opt => {
  const json = parse(opt);
  // @TODO apply filer and decorators
  log.dir(json);
};
// cmd options
const cli = meow(
  `
    Only require to pass the full path to the nsp on the server

    $ debuggerClient https://domain.com/nsp-name

    (TODO) more options will be available in the next release
  `,
  {
    x: 'exclude',
    d: 'display',
    c: 'config'
  }
);
/**
 * Main fn
 * @param {object} cli see above
 * @return {undefined} nothing
 */
const run = cli => {
  try {
    const nsp = io(cli.input[0]);
    // listen to the connection
    nsp.on('hello', msg => {
      log.info(chalk.yellow('[Server connected]'), msg);
    });
    // when device join the debugger channel
    nsp.on('memberjoin', member => {
      log.info(chalk.yellow('[member join]'));
      log.dir(member);
    });
    // listening to the broadcast
    nsp.on('broadcastdebug', msg => {
      if (msg.time && msg.error) {
        log.info(chalk.yellow('[Error msg]'), msg.time);
        // @TODO sort out the display
        display(msg.error);
      } else {
        display(msg);
      }
    });
  } catch (e) {
    log.error(e);
    log.error(chalk.red('You must provide the nsp!'));
  }
}
// start
run(cli);

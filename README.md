# server-io-debugger-client

separate client with stacktrace-js / socket.io-client to hijack the console.log for your remote debugging need (use with [server-io-core](https://gitlab.com/newbranltd/server-io-core) series of tools)

# Installation (For CLI client)

It's recommend to install this globally

```sh
$ npm i -g server-io-debugger-client
```

# Usage (For CLI client)

```sh
$ srvioDebuggerClient https://domain.com/nsp-name

```
Then wait for the connect message to make sure you are connected.
Then whenever your debugging target using the `console.log` or an Javascript error
happen, it will broadcast the message and received by the listening client.

---

# Installation for your browser app (or phonegap app)

You need to include the `server-io-debugger-client/src/main.js` and add to your
html document. Also you need to add [stacktrace-js]() and [socket.io-client]() to your
dependencies (and add to the html document if needed).

If you are using it with fancy JS UI library. You *might* get away with using

```js
import 'server-io-debugger-client';
```

Because it doesn't export anything. Try this at your own risk.

# Initialize your debugger

The debugger listen to a custom event `serverIoDebuggerStart` and expect a parameter `nsp` from the event.
What you can do is after your app init. You can trigger an event like this.

```js
var evt = new CustomEvent('serverIoDebuggerStart');
evt.nsp = 'http://your-server.com/your-nsp';
window.dispatchEvent(evt);
```

Once the client is connect to the debugger server. You will get a `serverIoDebuggerInited` event back.

So you can listen to this event and decided to what to do.

```js
window.addEventListener('serverIoDebuggerInited', function(e) {
  // do something with the e.msg
  // but don't use the console.log perhaps use console.info instead :)
});

```

Once the init is done. Whenever your app has `error`, `onunhandledrejection` event, or call the `console.log`
they will get send to the remote debugger server. Then you can use the cli client as describe above to observe
what is going on in your app. 

---

# TODO

* add more options to how this capture data
* add secondary logging option to log to file or database

MIT (c) NEWBRAN LTD 2018

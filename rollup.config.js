/**
 * Rollup config
 */
import fs from 'fs';
import { join } from 'path';
import buble from 'rollup-plugin-buble';
import uglify from 'rollup-plugin-uglify';

import replace from 'rollup-plugin-replace';
import commonjs from 'rollup-plugin-commonjs';

import nodeResolve from 'rollup-plugin-node-resolve';
import nodeGlobals from 'rollup-plugin-node-globals';
import builtins from 'rollup-plugin-node-builtins';

const fileName = 'server-io-debugger-client';

if (fs.existsSync(`./dist/${fileName}.js.map`)) {
  fs.unlinkSync(`./dist/${fileName}.js.map`);
};

const env = process.env.NODE_ENV;

let plugins = [
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({
    jsnext: true,
    main: true,
    browser: true
  }),
  commonjs(),
  nodeGlobals(),
  builtins(),
  (env === 'prod' || env === 'esm') && replace({ 'process.env.NODE_ENV': JSON.stringify('production') }),
  (env === 'prod' || env === 'esm') && uglify()
];

const file = env === 'main' ? `${fileName}.js` : `${fileName}.iife.js`;

let config = {
  input: join(__dirname, 'src', 'main.js'),
  output: {
    name: 'serverIoDebuggerClient',
    file: join(__dirname, 'dist', file),
    format: env === 'main' ? 'umd' : 'iife',
    sourcemap: env !== 'prod',
    globals: {
      superagent: 'superagent',
      'socket.io-client': 'io',
      'stacktrace-js': 'stackTrace'
    }
  },
  external: [
    "superagent",
    "handlebars",
    "tty",
    "socket.io-client",
    "stacktrace-js"
  ],
  plugins: plugins
};

export default config;
